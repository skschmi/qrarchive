import base64
import xml.etree.ElementTree as et
import os
import sys

print "QR Code Archiver"
print "Archive Usage:"
print "   $ python qr_archive.py <path_to_file> <path_to_output_directory>"
print "Unarchive Usage:"
print "   $ python qr_unarchive.py <path_to_input_directory> <path_to_output_directory>"

if(len(sys.argv) >= 3):

    # Init stuff
    i = 0
    totalpieces = -100
    filename = ""

    # The data that will be appended together
    alldata = bytearray([])

    # Use the C program "zbarimg" to read the QR code files
    while(i != totalpieces):
        #Decode the qr image and save it to a temporary file
        image_read_dir = sys.argv[1]
        decode_out_dir = sys.argv[2]
        image_fileext = ".png"
        decode_fileext = ".txt"
        image_filename = "file_part"+str(i).zfill(3)
        infile = image_read_dir+"/"+image_filename+image_fileext
        print "Working on importing ",infile,"..."
        outfile = decode_out_dir+"/junktemp"+decode_fileext
        system_command = "./zbarimg --raw "+infile+" > "+outfile
        os.system(system_command)
    
        # Open the file and read the contents
        f = open(outfile, "r")
        contents = f.read()
        # Remove any junk after the ending token
        end = contents.find("</bytes>") + 8
        contents = contents[:end]
        # Parse the value between the xml tags
        root = et.fromstring(contents[:end])
        if(i == 0):
            totalpieces = int(root.attrib["nparts"])
            filename = root.attrib["filename"]
            f.close()
            print "totalpieces: ", totalpieces
        # Decode the contents and add it to the buffer
        alldata.extend(bytearray(base64.b64decode(root.text)))
        i += 1

    # Save the file out to disk
    f = open("output/newfile_"+filename, "wb")
    f.write(alldata)
    f.close()


"""
# Read each QR code, and append the data together
while(i != totalpieces):
    image_filename = "qrimages/file_part"+str(i).zfill(3)+".png"
    if( qr.decode(image_filename) ):
        print "Image file name: ",image_filename
        print qr.data
        root = et.fromstring(qr.data)
        totalpieces = int(root.attrib["nparts"])
        filename = root.attrib["filename"]
        alldata.extend(bytearray(base64.b64decode(root.text)))
    else:
        print "Error: Failed to read QR code part",i
    print "Done with part ",i,"..."
    i += 1
    #todo: append the data to the file specified with "filename" *each loop*
    # in case there are multiple files
"""
