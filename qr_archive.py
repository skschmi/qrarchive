import qrtools
import base64
import numpy
import os
import sys

print "QR Code Archiver"
print "Archive Usage:"
print "   $ python qr_archive.py <path_to_file> <path_to_output_directory>"
print "Unarchive Usage:"
print "   $ python qr_unarchive.py <path_to_input_directory> <path_to_output_directory>"

if(len(sys.argv) >= 3):

    # Make a buffer for the file
    buf = bytearray([])

    #Make the qr object
    qr = qrtools.QR()

    #Read in the file
    filename = sys.argv[1]
    f = open(filename, "rb")
    if 'f' in locals():
        try:
            byte = f.read(1)
            while byte != "":
                buf.append(byte);
                byte = f.read(1)
        finally:
            f.close()

        # The max number of bytes in each qr code
        max_piece_size = 1024

        # Number qr codes to create
        npieces = int(numpy.ceil(float(len(buf)) / float(max_piece_size)))

        # Create each QR code to save each section of the data
        for i in xrange(0,npieces):
            print "Part ",i,"..."
            if( i < npieces-1 ):
                bufb64part = base64.b64encode(buf[i*max_piece_size:i*max_piece_size+max_piece_size])
            else:
                bufb64part = base64.b64encode(buf[i*max_piece_size:])
            xml_start = "<bytes format=\"base64\" filename=\""+os.path.basename(filename)+"\" part=\""+str(i+1)+"\" nparts=\""+str(npieces)+"\">"
            xml_end = "</bytes>"
            qr.data = xml_start+bufb64part+xml_end
            qr.encode(sys.argv[2]+"/file_part"+str(i).zfill(3)+".png")
    else:
        print "File ",filename," failed to open."

