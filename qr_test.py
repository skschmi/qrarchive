import qrtools
import base64
import numpy
import xml.etree.ElementTree as et
import os

# Init stuff
i = 3
totalpieces = -1
filename = ""
qr = qrtools.QR()

# The data that will be appended together
alldata = bytearray([])

image_filename = "qrimages/file_part"+str(i).zfill(3)+".png"
qr.decode(image_filename)
print "Image file name: ",image_filename
print qr.data

#root = et.fromstring(qr.data)
#totalpieces = int(root.attrib["nparts"])
#filename = root.attrib["filename"]
#alldata.extend(bytearray(base64.b64decode(root.text)))

